@extends('layouts.main')

@section('content')
    <div id="products">
        <div class="page">
            <h1 class="page__title">Products</h1>
            <div class="page__content">
                @foreach ($products as $product)
                <div class="product" data-id="{{ $product->id }}">
                    <div class="product__body">
                        <div class="product__name">{{ $product->name }}</div>
                        <div class="product__price">{{ $product->price }} руб.</div>
                        <div class="spacer spacer--h10"></div>
                        <button class="product__add-to-cart">Добавить в корзину</button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div id="cart"></div>
@endsection
