import React, { useEffect } from "react";
import { useRecoilState } from 'recoil';
import { cartState } from "../state";

export const Products = (props) => {
  const [cart, setCart] = useRecoilState(cartState);

  const addToCartButtons = document.querySelectorAll('#products .product__add-to-cart');
  const nativeAddToCart = e => {
    const product = e.currentTarget.closest('.product');
    setCart([...cart, product.dataset.id]);
  }

  useEffect(() => {
    if (addToCartButtons) {
      for (let i = 0, len = addToCartButtons.length; i < len; i++) {
        addToCartButtons[i].addEventListener('click', nativeAddToCart);
      }
    }

    return function cleanupListener() {
      if (addToCartButtons) {
        for (let i = 0, len = addToCartButtons.length; i < len; i++) {
          addToCartButtons[i].removeEventListener('click', nativeAddToCart);
        }
      }
    }
  }, [cart]);

  return null;
}
