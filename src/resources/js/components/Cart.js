import React, { useEffect } from "react";
import {
  useRecoilState,
} from 'recoil';
import { cartState } from '../state';

export const Cart = (props) => {
  const [cart] = useRecoilState(cartState);

  return (
    <div className="cart">
      <div className="cart__area">Cart <sup>{cart.length}</sup></div>
      <div className="cart__items">
        {
          // cart.map((item) => {
          //   return (
          //     <div className="cart-item" key={item.id}>
          //       <div className="cart-item__name">{item.name}</div>
          //       <div className="cart-item__price">{item.price}</div>
          //       <div className="cart-item__quantity">{item.quantity}</div>
          //     </div>
          //   );
          // })
        }
      </div>
    </div>
  );
}
