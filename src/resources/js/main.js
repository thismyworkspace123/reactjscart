import React from "react";
import ReactDOM, { createPortal } from "react-dom";
import { RecoilRoot } from "recoil";
import { Products } from "./components/Products";
import { Cart } from "./components/Cart";

// Mountpoint
const component = document.getElementById("mountpoint");
if (component) {
  // Cart
  const cartNode = document.getElementById('cart');

  ReactDOM.render(
    <RecoilRoot>
      <Products />
      {!cartNode ? null : createPortal(<Cart />, cartNode)}
    </RecoilRoot>,
    component
  );
}
